import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

  baseUrl: string = 'http://localhost:4000';
  httpOptions = {
    headers: new HttpHeaders({ 
      'Access-Control-Allow-Origin':'*'
    })
  };

  constructor(private http: HttpClient) { }

  follow(id: string): boolean {
    const params = id ? { params : new HttpParams().set('id', id) } : { };
    const observer = {
      next: x => console.log('Observer got a next value: ' + x),
      error: err => console.error('Observer got an error: ' + err),
      complete: () => console.log('Observer got a complete notification'),
    };
    let restResult = this.http.post(this.baseUrl + '/follow', id, params)
      .subscribe(observer);
    console.log('Calling API follow method result: ' + restResult);
    return true;
  }

  unfollow(id: string): boolean {
    const params = id ? { params : new HttpParams().set('id', id) } : { };
    const observer = {
      next: x => console.log('Observer got a next value: ' + x),
      error: err => console.error('Observer got an error: ' + err),
      complete: () => console.log('Observer got a complete notification'),
    };
    let restResult = this.http.post(this.baseUrl + '/unfollow', id, params)
      .subscribe(observer);
      console.log('Calling API unfollow method result: ' + restResult);
    return true;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
