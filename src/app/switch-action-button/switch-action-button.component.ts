import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FollowService } from '../services/follow.service';

@Component({
  selector: 'app-switch-action-button',
  templateUrl: './switch-action-button.component.html',
  styleUrls: ['./switch-action-button.component.css']
})
export class SwitchActionButtonComponent implements OnInit {

  action: string;
  value: boolean = false;
  uuid: string = '123e4567-e89b-12d3-a456-556642440000';

  constructor(private service: FollowService, private translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use(navigator.language);
  }

  ngOnInit(): void {
    this.switchAction();
  }

  onClick() {
    this.value = !this.value;
    this.switchAction();
    this.postAction();
  }

  private switchAction() {
    switch (this.value) {
      case false:
        this.action = 'follow';
        break;

      case true:
        this.action = 'unfollow';
        break;
      
      default:
        break;
    }
  }

  private postAction() {
    let result: boolean;
    if (this.value) {
      result = this.service.follow(this.uuid);
    } else {
      result = this.service.unfollow(this.uuid);
    }
    console.log("result = " + result);
  }
}
