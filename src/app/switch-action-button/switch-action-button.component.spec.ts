import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchActionButtonComponent } from './switch-action-button.component';

describe('SwitchActionButtonComponent', () => {
  let component: SwitchActionButtonComponent;
  let fixture: ComponentFixture<SwitchActionButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchActionButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
